public class InnoArrayList implements InnoList {
    private static final int DEFAULT_SIZE = 10;
    private int[] elements;
    private int count;
    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    private int[] copyArray() {
        int[] tempArray = new int[elements.length];
        return tempArray;
    }

    @Override
    public void insert(int index, int element) {
        if (count == elements.length) {
            resize();
        }
        int[] tempArray = copyArray();

        if (index >= 0 && index <= count) {
            for (int i = 0; i < index; i++) {
                tempArray[i] = elements[i];
            }
            tempArray[index] = element;

            for (int i = index; i < count; i++) {
                tempArray[i + 1] = elements[i];
            }
            elements = tempArray;
            count++;
        } else if (index > count) {
            index = count;

            for (int i = 0; i < count; i++) {
                tempArray[i] = elements[i];
            }
            tempArray[index] = element;
            elements = tempArray;
            count++;
        } else {
            System.err.println("Некорректный индекс!");
        }
    }
    @Override
    public void addToBegin(int element) {
        if (count == elements.length) {
            resize();
        }
        int[] tempArray = copyArray();
        tempArray[0] = element;

        for (int i = 0; i < tempArray.length - 1; i++) {
            tempArray[i + 1] = elements[i];
        }
        elements = tempArray;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count) {

            for (int i = 0; i < index; i++) {
                elements[i] = elements[i];
            }
            for (int i = index; i < count; i++) {
                elements[i] = elements[i + 1];
            }
            count--;
        } else if (index >= count) {
            System.err.println("Некорректный индекс!");
        } else if (index < 0) {
            System.err.println("Некорректный индекс!");
        }
    }

    @Override
    public void add(int element) {
        if (count == elements.length) {
            resize();
        }
        elements[count++] = element;
    }

    private void resize() {
        int newElements[] = new int[elements.length + elements.length / 2];

        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        int remoteCount = 0;

        for (int i = 0; i < elements.length; i++) {

            if (element == elements[i]) {
                remoteCount++;
                count--;
            } else {
                elements[i - remoteCount] = elements[i];
            }
        }
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < elements.length; i++) {
            if (element == elements[i]) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoArrayListIterator();
    }


    private class InnoArrayListIterator implements InnoIterator {

        private int currentPosition;

        @Override
        public int next() {
            int nextValue = elements[currentPosition];
            currentPosition++;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            return currentPosition < count;
        }
    }
}

