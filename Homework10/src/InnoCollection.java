public interface InnoCollection {
    void add(int element);
    void remove(int element);
    boolean contains(int element);
    int size();
    InnoIterator iterator();
}
