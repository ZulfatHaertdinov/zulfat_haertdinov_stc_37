public class InnoLinkedList implements InnoList{

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
        public void setNext(Node next) {
            this.next = next;
        }
    }
    private Node first;
    private Node last;
    private int count;

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        } else {
            return -1;
        }
    }
    @Override
    public void insert(int index, int element) {
        if (index >= 0 && index <= count) {
            Node newNode = new Node(element);
            Node current = first;

            if (index == 0) {
                first = newNode;
                first.next = current;
                count++;
                return;
            }

            for (int i = 1; i <= count; i++) {
                if (index == i) {
                    Node next = current.next;
                    current.setNext(newNode);
                    newNode.next = next;
                    count++;
                    break;
                }
                current = current.next;
            }
        } else if (index < 0) {
            System.err.println("Ошибка");
        } else {
            System.err.println("Ошибка");
        }
    }
    @Override
    public void addToBegin(int element) {
        Node newNode = new Node(element);
        Node current = first;

        if (first != null) {
            first = newNode;
            first.next = current;
            count++;
        } else {
            add(element);
        }
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count) {
            Node current = first;

            if (index == 0) {
                first = current.next;
                count--;
                return;
            }

            for (int i = 1; i < count; i++) {
                if (index == i) {
                    Node next = current.next.next;
                    current.setNext(next);
                    count--;
                    break;
                }
                current = current.next;
            }
        } else if (index < 0) {
            System.err.println("Ошибка! Некорректный индекс!");
        } else {
            System.err.println("Ошибка! Некорректный индекс!");
        }
    }
    @Override
    public void add(int element) {
        Node newNode = new Node(element);

        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {
        Node current = first;
        Node previous = first;

        for (int i = 0; i < count; i++) {
            if (element == current.value && i == 0) {
                first = current.next;
                count--;
            } else if (element == current.value) {
                previous.setNext(current.next);
                count--;
                break;
            }
            previous = current;
            current = current.next;
        }
    }

    @Override
    public boolean contains(int element) {
        Node current = first;

        for (int i = 0; i < count; i++) {
            if (element == current.value) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {
        private Node next;

        InnoLinkedListIterator() {
            this.next = first;
        }

        @Override
        public int next() {
            int nextValue = next.value;
            next = next.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            return next != null;
        }
    }
}


