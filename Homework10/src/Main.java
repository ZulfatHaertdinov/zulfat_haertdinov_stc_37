public class Main {
    public static void main(String[] args) {
        InnoList list1 = new InnoLinkedList();
        list1.add(99);
        list1.add(100);
        list1.add(160);
        System.out.println(list1.get(33));
        list1.insert(1, 33);
        list1.insert(2, 60);
        list1.addToBegin(2);
        list1.removeByIndex(1);
        list1.remove(2);
        System.out.println(list1.contains(0));
        InnoIterator iterator1 = list1.iterator();
        while (iterator1.hasNext()) {
            System.out.print(iterator1.next() + " ");
        }
        InnoList list2 = new InnoArrayList();
        list2.add(22);
        list2.add(256);
        list2.add(70);
        System.out.println(list2.get(1));
        list2.insert(0, 33);
        list2.insert(2, 80);
        list2.addToBegin(55);
        list2.removeByIndex(3);
        list2.remove(33);
        System.out.println(list2.contains(256));
        InnoIterator iterator2 = list2.iterator();
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
    }
}

