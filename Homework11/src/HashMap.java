public class HashMap <K, V> implements Map<K,V> {
        private static final int DEFAULT_SIZE = 16;
        private MapEntry<K, V>[] entries;
        public HashMap() {
            this.entries = new MapEntry[DEFAULT_SIZE];
        }
        private static class MapEntry<K, V> {
            K key;
            V value;
            MapEntry<K, V> next;
            public MapEntry(K key, V value) {
                this.key = key;
                this.value = value;
            }
        }
        private int getIndex(K key) {
            int index = key.hashCode() & (entries.length - 1);
            return index;
        }
        @Override
        public void put(K key, V value) {
            int index = getIndex(key);

            if (entries[index] != null) {
                MapEntry<K,V> current = entries[index];

                while (current != null) {

                    if (current.key.equals(key)) {
                        current.value = value;
                        return;
                    }
                    current = current.next;
                }
                MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
                newMapEntry.next = entries[index];
                entries[index] = newMapEntry;
            } else {
                entries[index] = new MapEntry<>(key, value);
            }
        }
        @Override
        public V get(K key) {
            int index = getIndex(key);
            MapEntry<K, V> current = entries[index];
            while (current != null) {
                if (!current.key.equals(key)) {
                    current = current.next;
                } else {
                    return current.value;
                }
            }
            return null;
        }
    }

