import java.util.Scanner;

class Task01 {
public static void main(String args[]) {
	Scanner scanner = new Scanner(System.in);
	int n = scanner.nextInt();
	int array[] = new int[n];
	int arraySum = 0;
	for (int j = 0; j < n; j++) {
		array[j] = scanner.nextInt();
		arraySum = arraySum + array[j];
	}
	System.out.println(arraySum); 
}
}