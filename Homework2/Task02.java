import java.util.Scanner;
import java.util.Arrays;

class Task02 {
public static void main(String args[]) {
	Scanner scanner = new Scanner(System.in);
	int n = scanner.nextInt();
	int array[] = new int[n];
	for (int j = 0; j < n; j++) {
		array[j] = scanner.nextInt();
	}
	int temp = 0;
	for (int i = 0; i < n/2; i++) {
		temp = array[n-i-1];
		array[n-i-1] = array[i];
		array[i] = temp;
	}
	System.out.println(Arrays.toString(array)); 
}
}

