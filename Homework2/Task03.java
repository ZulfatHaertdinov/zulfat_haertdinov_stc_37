import java.util.Scanner;
import java.util.Arrays;

class Task03 {
public static void main(String args[]) {
	Scanner scanner = new Scanner(System.in);
	int n = scanner.nextInt();
	int array[] = new int[n];
	for (int j = 0; j < n; j++) {
		array[j] = scanner.nextInt();
	}
	double arrayAverage = 0;
	if (n > 0) {
		double sum = 0;
		for (int i = 0; i < n; i++) {
			sum = sum + array[i];
		}
		arrayAverage = sum / n;
	}
	System.out.println(arrayAverage);
}
}