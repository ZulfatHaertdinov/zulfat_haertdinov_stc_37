import java.util.Scanner;
import java.util.Arrays;

class Task04 {
public static void main(String args[]) {
	Scanner scanner = new Scanner(System.in);
	int n = scanner.nextInt();
	int array[] = new int[n];
	for (int j = 0; j < n; j++) {
		array[j] = scanner.nextInt();
	}
	int min = array[0];
	int max = array[0];
	int temp;
	for (int i = 0; i < array.length; i++) {
		if (array[i] < array[min]) {
		min = i;
		}
		if (array[i] > array[max]) {
		max = i;
		}
	}
	temp = array[min];
	array[min] = array[max];
	array[max] = temp;
	System.out.println(Arrays.toString(array));
}
}

