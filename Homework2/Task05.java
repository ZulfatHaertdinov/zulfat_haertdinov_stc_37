import java.util.Scanner;
import java.util.Arrays;

class Task05 {
public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();
    int array[] = new int[n];
    for (int j = 0; j < n; j++) {
        array[j] = scanner.nextInt();
    }
    boolean isSorted = false;
    int buf;
    while(!isSorted) {
        isSorted = true;
        for (int i = 0; i < n-1; i++) {
            if(array[i] > array[i+1]){
            isSorted = false;
            buf = array[i];
            array[i] = array[i+1];
            array[i+1] = buf;
            }
        }
    }
    System.out.println(Arrays.toString(array));
}
}