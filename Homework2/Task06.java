import java.util.Scanner;
import java.util.Arrays;

class Task06 {
public static void main(String args[]) {
	int array[] = {4, 2, 3, 5, 7, 6};
	int number = 0;
	double multiplier = 1;
	for(int i = array.length-1; i >=0 ;i--) {
		number += array[i] * multiplier;
		multiplier = Math.pow(10, String.valueOf(number).length());
	}
	System.out.println(number); 
}
}
