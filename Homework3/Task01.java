import java.util.Scanner;
import java.util.Arrays;

class Task01 {
    public static void main(String args[]) {
      int array[] = {4, 2, 3, 5, 7, 6};
      
      sumArray(array);
      swap();
      arrayAverage();
      swapMaxAndMin();
      bufArray();
      multiplierArrayInNumber();
    }
  	
    public static void sumArray(int[] array) {
    	int sum = 0;
      for (int i = 0; i<array.length; i++) {
        sum = sum + array[i];
      }
     
      System.out.println(sum);

    }
        
    public static void swap() {
      Scanner scanner = new Scanner(System.in);
      int n = scanner.nextInt();
      int array[] = new int[n];
      for (int j = 0; j < n; j++) {
        array[j] = scanner.nextInt();
      }
      int temp = 0;
      for (int i = 0; i < n/2; i++) {
        temp = array[n-i-1];
        array[n-i-1] = array[i];
        array[i] = temp;
      }

      System.out.println(Arrays.toString(array)); 
    } 
    public static void arrayAverage () {
      Scanner scanner = new Scanner(System.in);
      int n = scanner.nextInt();
      int array[] = new int[n];
      for (int j = 0; j < n; j++) {
        array[j] = scanner.nextInt();
      }
      double arrayAverage = 0;
      if (n > 0) {
        double sum = 0;
        for (int i = 0; i < n; i++) {
          sum = sum + array[i];
        }
      arrayAverage = sum / n;
      }

      System.out.println(arrayAverage);
    }
    public static void swapMaxAndMin () {
      int array[] = {4, 3, 2, 1, 0};
      int min = array[0];
      int max = array[0];
      int temp;
      for (int i = 0; i < array.length; i++) {
        if (array[i] < array[min]) {
          min = i;
      }
        if (array[i] > array[max]) {
          max = i;
        }
      }
      temp = array[min];
      array[min] = array[max];
      array[max] = temp;

      System.out.println(Arrays.toString(array));
    }
    public static void bufArray () {
      Scanner scanner = new Scanner(System.in);
      int n = scanner.nextInt();
      int array[] = new int[n];

      for (int j = 0; j < n; j++) {
        array[j] = scanner.nextInt();
      }
      boolean isSorted = false;
      int buf;
      while(!isSorted) {
        isSorted = true;
        for (int i = 0; i < n-1; i++) {
          if(array[i] > array[i+1]) {
            isSorted = false;
            buf = array[i];
            array[i] = array[i+1];
            array[i+1] = buf;
          }
        }
      }
      System.out.println(Arrays.toString(array));
    }
    public static void multiplierArrayInNumber () {
      int array[] = {4, 2, 3, 5, 7, 6};
      int number = 0;
      double multiplier = 1;
        for(int i = array.length-1; i >=0 ;i--) {
          number += array[i] * multiplier;
          multiplier = Math.pow(10, String.valueOf(number).length());
        }
      System.out.println(number); 
    }
}