import java.util.Random;

public class Channel {
    private String nameChannel;
    public Program[] program;

    public String getNameChannel() {
        return nameChannel;
    }

    public Channel(String nameChannel, Program[] program) {
        this.nameChannel = nameChannel;
        this.program = program;
    }

    public void randoms() {
        Random random = new Random();
        int index = random.nextInt(program.length);
        String randomProgram = program[index].getNameProgram();
        System.out.println(randomProgram);
    }
}
