import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Program[]programs = new Program[] {
            new Program("Симсоны"),
            new Program("Супермен"),
            new Program("Аквамен"),
    };

    Program[] programs2 = new Program[] {
            new Program("Бэтмен"),
            new Program("Мстители"),
            new Program("Вечерний ургант"),
    };

    Program[] programs3 = new Program[] {
            new Program("Три богатыря"),
            new Program("Русалка"),
            new Program("Время"),
    };

    Channel[] channels = new Channel[] {
            new Channel("Первый канал", programs),
            new Channel("Пятница", programs2),
            new Channel("Рен тв", programs3),
    };

    Tv tv = new Tv(channels);
    RemoteController remoteController = new RemoteController(tv);
    Scanner scanner = new Scanner(System.in);
    int a = scanner.nextInt();
    if (a >= channels.length)  {
      tv.addChannel();
    } else {
      remoteController.channelAccess(a);
    }
  }
}

