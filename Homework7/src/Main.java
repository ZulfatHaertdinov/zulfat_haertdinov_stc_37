public class Main {

    public static void main(String[] args) {

        User user =  new User.Builder()
                .firstName("zulat")
                .lastName("Haertdinov")
                .age(26)
                .isWorker(true)
                .build();
    }
}
