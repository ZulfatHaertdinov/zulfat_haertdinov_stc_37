
public class User {
    private final String firstName;
    private final String lastName;
    private final int age;
    private final boolean isWorker;

    public static class Builder {

        private String firstName = null;
        private String lastName = null;
        private int age = 0;
        private boolean isWorker = false;

        public Builder() {

        }
        public Builder firstName(String val) {
            firstName = val;
            return this;
        }
        public Builder lastName(String val) {
            lastName = val;
            return this;
        }
        public Builder age(int val) {
            age = val;
            return this;
        }

        public Builder isWorker(boolean val) {
            isWorker = val;
            return this;
        }
        public User build() {
            return new User(this);
        }
    }

    private User(Builder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        age = builder.age;
        isWorker = builder.isWorker;
    }
}