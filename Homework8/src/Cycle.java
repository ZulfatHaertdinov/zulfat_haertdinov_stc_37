public class Cycle extends GeometricFigure{
    double radius;
    public Cycle(double length, double width, double radius) {
        super(length, width);
        this.radius = radius;
    }
    public double square() {
        return (radius * radius)*Math.PI;
    }
    public double perimeter() {
        return  2*radius* Math.PI ;
    }
}



