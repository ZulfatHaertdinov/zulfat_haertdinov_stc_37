public class Ellipse extends Cycle {
    double semiMajorAxis;
    double semiMinorAxis;
    public Ellipse(double length, double width, double semiMajorAxis, double semiMinorAxis) {
        super(length, width, 0);
        this.semiMajorAxis = semiMajorAxis;
        this.semiMinorAxis = semiMinorAxis;
    }
    public double square() {
        return semiMajorAxis * semiMinorAxis * Math.PI;
    }
    public double perimeter() {
       return  4*(Math.PI*semiMajorAxis*semiMinorAxis+(semiMajorAxis-semiMinorAxis))/semiMajorAxis+semiMinorAxis;
    }
}

