public abstract class GeometricFigure implements Scalable,Relocatable {
    protected double length;
    protected double width;

    public GeometricFigure (double length, double width) {
        this.length = length;
        this.width = width;
    }
    public abstract double square();
    public abstract double perimeter();

    public double scalable (double scale, double length, double width) {
        return scale*(length*width);
    }

    public int relocatable (int x, int y) {
        int position = 1;
        return position + (x * y);
    }

}

