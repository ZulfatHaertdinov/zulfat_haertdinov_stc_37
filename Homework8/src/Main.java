public class Main {
    public static void main(String[] args) {
        GeometricFigure ellipse = new Ellipse(4,4,3,3);
		System.out.println(ellipse.square());
		System.out.println(ellipse.perimeter());
		System.out.println(ellipse.scalable(3,3,4));
		System.out.println(ellipse.relocatable(2,4));
        GeometricFigure cycle = new Cycle(4,3,3);
		System.out.println(cycle.square());
		System.out.println(cycle.perimeter());
		System.out.println(cycle.scalable(3,3,4));
		System.out.println(cycle.relocatable(2,4));
        GeometricFigure rectangle = new Rectangle(3, 4);
		System.out.println(rectangle.square());
		System.out.println(rectangle.perimeter());
		System.out.println(rectangle.scalable(3,4,3));
		System.out.println(rectangle.relocatable(2,4));
        GeometricFigure square = new Square(3);
		System.out.println(square.square());
		System.out.println(square.perimeter());
		System.out.println(square.scalable(3,4,3));
		System.out.println(square.relocatable(2,4));

	}
}
