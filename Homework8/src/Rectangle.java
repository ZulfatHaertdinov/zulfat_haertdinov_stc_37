public class Rectangle extends GeometricFigure {
    public Rectangle(double length, double width) {
        super(length, width);
    }
    public double square() {
        return length * width;
    };
    public double perimeter() {
        return (length * width)*2;
    }

}

