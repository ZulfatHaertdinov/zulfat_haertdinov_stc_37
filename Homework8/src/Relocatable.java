public interface Relocatable {
    int relocatable( int x, int y);
}
