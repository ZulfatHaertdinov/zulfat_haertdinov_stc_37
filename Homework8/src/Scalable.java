public interface Scalable {
  double scalable(double scale,double length, double width);

}
