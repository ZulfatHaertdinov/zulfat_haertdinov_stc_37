public class Square extends Rectangle {
    public Square(double length) {
        super(length, 0);
    }
    public double perimeter() {
        return length *4;
    }
}
