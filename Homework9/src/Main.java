import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        NumbersProcess expandTheNumber = number -> {
        int mirror = 0;
        while (number != 0) {
            mirror = mirror * 10 + (number % 10);
            number = number / 10;
        }
        return mirror;
        };
        NumbersProcess removeZerosFromNumber = number -> {
            while (number % 10 == 0) {
                number = number / 10;
            }
            return number;
        };
        NumbersProcess replaceOddDigits = number -> {
            if (number % 2 == 0) {
                return number;
            } else {
                return number - 1;
            }
        };
        StringsProcess expandTheString = process -> {
            char[] chs = process.toCharArray();
            int i=0, j=chs.length-1;
            while (i < j) {
                char t = chs[i];
                chs[i] = chs[j];
                chs[j] = t;
                i++; j--;
            }
            return String.valueOf(chs);
        };
        StringsProcess removeAllNumbersFromTheString = process -> {
            String dataString = process;
            dataString = dataString.replaceAll("\\d", "");
            return dataString;
        };
        StringsProcess makeAllSmallLettersBIG = process -> {
            return process.toUpperCase();
        };
        int a [] = {100,224,31,5};
        String b [] = {"Abc12", "Bb2c", "cbc", "Dfs"};
        NumbersAndStringProcessor processor = new NumbersAndStringProcessor (a,b);
        System.out.println(Arrays.toString(processor.process(expandTheNumber)));
        System.out.println(Arrays.toString(processor.process(removeZerosFromNumber)));
        System.out.println(Arrays.toString(processor.process(replaceOddDigits)));
        System.out.println(Arrays.toString(processor.process(expandTheString)));
        System.out.println(Arrays.toString(processor.process(removeAllNumbersFromTheString)));
        System.out.println(Arrays.toString(processor.process(makeAllSmallLettersBIG)));
    }
}
