public class NumbersAndStringProcessor {
        private  int[] num;
        private  String[] str;
        public NumbersAndStringProcessor(int[] num, String[] str) {
            this.num = num;
            this.str = str;
        }
        public int[] process(NumbersProcess process1) {
            int[] result = new int[num.length];
            for (int i = 0; i < num.length; i++) {
                result[i] = process1.process(num[i]);
            }
            return result;
        }
        public String[] process(StringsProcess process1) {
            String[] result = new String[str.length];
            for (int i = 0; i < str.length; i++) {
                result[i] = process1.process(str[i]);
            }
            return result;
        }
    }
